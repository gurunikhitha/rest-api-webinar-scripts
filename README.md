# commits_per_repo.py

### Description
Generates a CSV with the number of commits in each repository of each project.
The resulting file will have 3 columns:

1. project key 
2. repository slug 
3. number of commits

### Requirements 
To make this script work, you’ll need to install the [requests](https://requests.readthedocs.io/en/master/) module in advance, the [csv](https://docs.python.org/3/library/csv.html) and [sys](https://docs.python.org/3/library/sys.html) modules are available in Python out of the box. 
You need to pass 3 arguments to the script when executed: **the URL of your Bitbucket, login, password**.

### Running the script
`py commits_per_repo.py https://bitbucket.your-company-name.com login password`

---

# global_commits_statistics.py

### Description
Generates a CSV with the number of commits in each repository of each project made during the specified period, grouped by users.
The resulting file will have 4 columns:

1. project key 
2. repository slug 
3. author's email
4. number of author's commits (sorted from largest to smallest per repo)

### Requirements 
To make this script work, you’ll need to install the [requests](https://requests.readthedocs.io/en/master/) module in advance, the [csv](https://docs.python.org/3/library/csv.html), [sys](https://docs.python.org/3/library/sys.html) and [itertools](https://docs.python.org/3/library/itertools.html) modules are available in Python out of the box. 
You need to pass 5 arguments to the script when executed: **the URL of your Bitbucket, login, password, start date, end date**.

### Running the script
`py global_commits_statistics.py https://bitbucket.your-company-name.com login password 1970-12-01 1970-12-31`

---

# loc_per_project.py

### Description
Generates a CSV with the total number of lines of code (LOC) in each project.
The resulting file will have 2 columns:

1. project key 
2. total LOC

### Requirements 
To make this script work, you’ll need to install the [requests](https://requests.readthedocs.io/en/master/) module in advance, the [csv](https://docs.python.org/3/library/csv.html) and [sys](https://docs.python.org/3/library/sys.html) modules are available in Python out of the box. 
You need to pass 3 arguments to the script when executed: **the URL of your Bitbucket, login, password**.

### Running the script
`py loc_per_project.py https://bitbucket.your-company-name.com login password`

---

# loc_per_repo.py

### Description
Generates a CSV with the total number of lines of code (LOC) in each repository of the specified project.
The resulting file will have 2 columns:

1. repo slug 
2. total LOC

### Requirements 
To make this script work, you’ll need to install the [requests](https://requests.readthedocs.io/en/master/) module in advance, the [csv](https://docs.python.org/3/library/csv.html) and [sys](https://docs.python.org/3/library/sys.html) modules are available in Python out of the box. 
You need to pass 3 arguments to the script when executed: **the URL of your Bitbucket, login, password, project key**.

### Running the script
`py loc_per_repo.py https://bitbucket.your-company-name.com login password PRKEY`

---

# pr_cycle_time.py

### Description
Generates a CSV report with the activities time points for pull requests made during the specified period and the following metrics calculated:
1. PR cycle time (from the first commit to merge)
2. Time to open (from the first commit to open)
3. Time waiting for review (from open to the first comment)
4. Time to approve (from the first comment to approved)
5. Time to merge (from approved to merge)


The resulting file will have 14 columns:

1. pull request ID
2. title
3. author's email
4. state
5. date of the first commit
6. pull request creation date
7. date of the first comment
8. date of the last approval
9. pull request close date
10. cycle time in days
11. time to open in days
12. time to review in days
13. time to approve in days
14. time to merge in days


### Requirements

To make this script work, you’ll need to pre-install the [requests](https://requests.readthedocs.io/en/master/) and [dateutil](https://dateutil.readthedocs.io/en/stable/) modules. The csv, sys, and datetime modules are available in Python out of the box. You need to pass the following arguments to the script when executed: **the URL of your Bitbucket, login, password, project key, repository slug, since date (to include PRs created after), until date (to include PRs created before).**

### Running the script
`py pr_cycle_time.py https://bitbucket.your-company-name.com login password PRKEY repo-slug 2020-11-30 2021-02-01`

---

# pull_requests_per_repo.py

### Description
Generates a CSV with the number of pull requests in each repository of each project made during the specified period.
The resulting file will have 6 columns:

1. project key 
2. repository slug
3. overall number of pull requests
4. number of open pull requests
5. number of merged pull requests
6. number of declined pull requests

### Requirements
To make this script work, you’ll need to install the [requests](https://requests.readthedocs.io/en/master/) module in advance, the [csv](https://docs.python.org/3/library/csv.html) and [sys](https://docs.python.org/3/library/sys.html) modules are available in Python out of the box. 
You need to pass 5 arguments to the script when executed: **the URL of your Bitbucket, login, password, start date, end date**.

### Running the script
`py pull_requests_per_repo.py https://bitbucket.your-company-name.com login password 1970-12-01 1970-12-31`

---

# user_commits_statistics.py

### Description
Generates a CSV with the number of users' commits and lines of code added/deleted in all projects during the specified period.
The resulting file will have 4 columns:

1. user slug
2. number of commits
3. number of lines added
4. number of lines deleted

### Requirements
To make this script work, you’ll need to install the [requests](https://requests.readthedocs.io/en/master/) module in advance, the [csv](https://docs.python.org/3/library/csv.html) and [sys](https://docs.python.org/3/library/sys.html) modules are available in Python out of the box. 
You need to pass 5 arguments to the script when executed: **the URL of your Bitbucket, login, password, start date, end date**.

### Running the script
`py user_commits_statistics.py https://bitbucket.your-company-name.com login password 1970-12-01 1970-12-31`

---

# user_commits_statistics_pandas.py

### Description
Generates a CSV with the number of users' commits made and lines of code added/deleted in all projects during the specified period.
The resulting file will have 4 columns:

1. author's email
2. number of commits
3. number of lines added
4. number of lines deleted

### Requirements
To make this script work, you’ll need to install the [pandas](https://pypi.org/project/pandas/) library in advance. 
You need to pass 5 arguments to the script when executed: **the URL of your Bitbucket, login, password, start date, end date**.

### Running the script
`py user_commits_statistics_pandas.py https://bitbucket.your-company-name.com login password 1970-12-01 1970-12-31`