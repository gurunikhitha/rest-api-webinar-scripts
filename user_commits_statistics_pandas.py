import pandas as pd
import requests
import sys

bitbucket_url = sys.argv[1]
login = sys.argv[2]
password = sys.argv[3]
since = sys.argv[4]
until = sys.argv[5]

response = requests.get(bitbucket_url + '/rest/awesome-graphs-api/latest/commits/export/csv', \
        params={'sinceDate': since, 'untilDate': until}, auth=(login, password))

with open('user_commits_statistics.csv', 'wb') as file:
    file.write(response.content)

df = pd.read_csv("user_commits_statistics.csv")

sorted_df = df.groupby('Git Author Email', as_index=False) \
    .agg({'Commit Hash': 'count', 'Added Lines': 'sum', 'Deleted Lines': 'sum'}) \
    .rename(columns={"Commit Hash": "Commits"}) \
    .sort_values(by='Commits', ascending=False)

sorted_df.to_csv('user_commits_statistics.csv', index=False)

print('The resulting CSV file is saved to the current folder.')
