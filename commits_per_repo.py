import requests
import csv
import sys

bitbucket_url = sys.argv[1]
login = sys.argv[2]
password = sys.argv[3]

bb_api_url = bitbucket_url + '/rest/api/latest'
ag_api_url = bitbucket_url + '/rest/awesome-graphs-api/latest'

s = requests.Session()
s.auth = (login, password)


def get_project_keys():

    projects = []

    is_last_page = False

    while not is_last_page:

        request_url = bb_api_url + '/projects'
        response = s.get(request_url, params={'start': len(projects), 'limit': 25}).json()

        for project in response['values']:
            projects.append(project['key'])

        is_last_page = response['isLastPage']

    return projects


def get_repos(project_key):

    repos = list()

    is_last_page = False

    while not is_last_page:

        request_url = bb_api_url + '/projects/' + project_key + '/repos'
        response = s.get(request_url, params={'start': len(repos), 'limit': 25}).json()

        for repo in response['values']:
            repos.append(repo['slug'])

        is_last_page = response['isLastPage']

    return repos


def get_commits(project_key, repo_slug):

    url = bitbucket_url + '/rest/awesome-graphs-api/latest/projects/' + project_key + '/repos/' + repo_slug + \
          '/commits/statistics'
    response = s.get(url).json()

    commits = response['commits']

    return commits


with open('commits_per_repo.csv', mode='a', newline='') as report_file:
    report_writer = csv.writer(report_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    report_writer.writerow(['project_key', 'repo_slug', 'commits'])

    for project_key in get_project_keys():
        print('Processing project', project_key)

        for repo_slug in get_repos(project_key):
            commits = get_commits(project_key, repo_slug)
            report_writer.writerow([project_key, repo_slug, commits])

print('The resulting CSV file is saved to the current folder.')