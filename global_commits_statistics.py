import requests
import csv
import itertools
import sys

bitbucket_url = sys.argv[1]
login = sys.argv[2]
password = sys.argv[3]
since = sys.argv[4]
until = sys.argv[5]

s = requests.Session()
s.auth = (login, password)


class Commit:

    def __init__(self, project, repo, email):
        self.project = project
        self.repo = repo
        self.email = email


class ProcessedCommitData:

    def __init__(self, project_key, repo_slug, author_email, commits):
        self.project_key = project_key
        self.repo_slug = repo_slug
        self.author_email = author_email
        self.commits = commits


def get_commits():

    commit_list = []

    is_last_page = False

    while not is_last_page:

        url = bitbucket_url + '/rest/awesome-graphs-api/latest/commits'

        response = s.get(url, params={'sinceDate': since, 'untilDate': until, 'start': len(commit_list), 'limit': 1000}).json()

        for commit_data in response['values']:

            project = commit_data['repository']['project']['key']
            repo = commit_data['repository']['slug']
            email = commit_data['author']['emailAddress']

            commit_list.append(Commit(project, repo, email))

        is_last_page = response['isLastPage']

    return commit_list


def get_details(data):
    return data.project, data.repo, data.email


with open('global_commits_statistics_{}_{}.csv'.format(since, until), mode='a', newline='') as report_file:

    report_writer = csv.writer(report_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    report_writer.writerow(['project_key', 'repo_slug', 'author_email', 'commits'])

    global_commit_list = []

    for commit in get_commits():
        global_commit_list.append(commit)

    global_commit_list.sort(key=get_details)

    grouped_without_commits_number = itertools.groupby(global_commit_list, get_details)

    grouped_result = []

    for key, group in grouped_without_commits_number:
        project_key = key[0]
        repo_slug = key[1]
        author_email = key[2]
        commits = len(list(group))
        grouped_result.append(ProcessedCommitData(project_key, repo_slug, author_email, commits))

    grouped_result.sort(key=lambda processed_data: (processed_data.project_key, processed_data.repo_slug, -processed_data.commits))

    for result in grouped_result:
        report_writer.writerow([result.project_key, result.repo_slug, result.author_email, result.commits])

print('The resulting CSV file is saved to the current folder.')
