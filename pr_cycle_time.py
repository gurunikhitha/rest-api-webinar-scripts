import sys
import requests
import csv
from dateutil import parser
from datetime import datetime

bitbucket_url = sys.argv[1]
login = sys.argv[2]
password = sys.argv[3]
project = sys.argv[4]
repository = sys.argv[5]
since = sys.argv[6]
until = sys.argv[7]

s = requests.Session()
s.auth = (login, password)


class PullRequest:

    def __init__(self, pr_id, title, author, state, created, closed):
        self.pr_id = pr_id
        self.title = title
        self.author = author
        self.state = state
        self.created = created
        self.closed = closed


def parse_date_ag_rest(date):
    return parser.isoparse(date).replace(tzinfo=None, microsecond=0)


def get_date_from_timestamp(timestamp):
    return datetime.fromtimestamp(timestamp / 1000).replace(microsecond=0)


def subtract_dates(minuend, subtrahend):
    if minuend is None or subtrahend is None:
        return None
    else:
        return round(((minuend - subtrahend).total_seconds() / 86400), 2)


def get_pull_requests():

    pull_request_list = []

    get_prs_url = bitbucket_url + '/rest/awesome-graphs-api/latest/projects/' + project + '/repos/' + repository \
        + '/pull-requests'

    is_last_page = False

    while not is_last_page:

        response = s.get(get_prs_url, params={'start': len(pull_request_list), 'limit': 1000,
                                              'sinceDate': since, 'untilDate': until}).json()

        for pr_details in response['values']:

            pd_id = pr_details['id']
            title = pr_details['title']
            author = pr_details['author']['user']['emailAddress']
            state = pr_details['state']
            created = parse_date_ag_rest(pr_details['createdDate'])

            if pr_details['closed'] is True:
                closed = parse_date_ag_rest(pr_details['closedDate'])
            else:
                closed = None

            pull_request_list.append(PullRequest(pd_id, title, author, state, created, closed))

        is_last_page = response['isLastPage']

    return pull_request_list


def get_first_commit_time(pull_request):

    commit_dates = []

    commits_url = bitbucket_url + '/rest/api/latest/projects/' + project + '/repos/' + repository + '/pull-requests/' \
        + str(pull_request.pr_id) + '/commits'

    is_last_page = False

    while not is_last_page:

        commits_response = s.get(commits_url, params={'start': len(commit_dates), 'limit': 500}).json()

        for commit in commits_response['values']:
            commit_timestamp = commit['authorTimestamp']
            commit_dates.append(get_date_from_timestamp(commit_timestamp))

        is_last_page = commits_response['isLastPage']

    if not commit_dates:
        first_commit = None
    else:
        first_commit = commit_dates[-1]

    return first_commit


def get_pr_activities(pull_request):

    counter = 0
    comment_dates = []
    approval_dates = []

    pr_url = bitbucket_url + '/rest/api/latest/projects/' + project + '/repos/' + repository + '/pull-requests/' \
        + str(pull_request.pr_id) + '/activities'

    is_last_page = False

    while not is_last_page:

        pr_response = s.get(pr_url, params={'start': counter, 'limit': 500}).json()

        for pr_activity in pr_response['values']:

            counter += 1

            if pr_activity['action'] == 'COMMENTED':
                comment_timestamp = pr_activity['comment']['createdDate']
                comment_dates.append(get_date_from_timestamp(comment_timestamp))
            elif pr_activity['action'] == 'APPROVED':
                approval_timestamp = pr_activity['createdDate']
                approval_dates.append(get_date_from_timestamp(approval_timestamp))

            is_last_page = pr_response['isLastPage']

    if not comment_dates:
        first_comment_date = None
    else:
        first_comment_date = comment_dates[-1]

    if not approval_dates:
        approval_time = None
    else:
        approval_time = approval_dates[0]

    return first_comment_date, approval_time


print('Collecting a list of pull requests from the repository', repository)

with open(f'{project}_{repository}_prs_cycle_time_{since}_{until}.csv', mode='a', newline='') as report_file:
    report_writer = csv.writer(report_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    report_writer.writerow(['id',
                            'title',
                            'author',
                            'state',
                            'first_commit',
                            'created',
                            'first_comment',
                            'approved',
                            'closed',
                            'cycle_time_d',
                            'time_to_open_d',
                            'time_to_review_d',
                            'time_to_approve_d',
                            'time_to_merge_d'])

    for pull_request in get_pull_requests():

        print('Processing pull request', pull_request.pr_id)

        first_commit_time = get_first_commit_time(pull_request)

        first_comment, approval = get_pr_activities(pull_request)

        cycle_time = subtract_dates(pull_request.closed, first_commit_time)

        time_to_open = subtract_dates(pull_request.created, first_commit_time)

        time_to_review = subtract_dates(first_comment, pull_request.created)

        time_to_approve = subtract_dates(approval, first_comment)

        time_to_merge = subtract_dates(pull_request.closed, approval)

        report_writer.writerow([pull_request.pr_id,
                                pull_request.title,
                                pull_request.author,
                                pull_request.state,
                                first_commit_time,
                                pull_request.created,
                                first_comment,
                                approval,
                                pull_request.closed,
                                cycle_time,
                                time_to_open,
                                time_to_review,
                                time_to_approve,
                                time_to_merge])

print('The resulting CSV file is saved to the current folder.')

